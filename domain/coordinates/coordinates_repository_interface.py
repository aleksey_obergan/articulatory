
class CoordinatesRepositoryInterface:

    def __init__(self):
        pass

    def get_coordinates_collection(self, data):
        raise Exception('Method get_coordinates_collection must be implemented')

    def get_coordinates_snapshot(self, data):
        raise Exception('Method get_coordinates_snapshot must be implemented')
