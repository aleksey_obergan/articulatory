class CoordinatesSnapshot:

    # T1 - tongue sensor 1
    # T2 - tongue sensor 2
    # T3 - tongue sensor 3
    # UL - upper lip sensor
    # LL - lower lip sensor
    # J - jaw sensor
    def __init__(
            self,
            T1x,
            T1y,
            T2x,
            T2y,
            T3x,
            T3y,
            ULx,
            ULy,
            LLx,
            LLy,
            Jx,
            Jy
    ):
        self.__T1x = T1x
        self.__T1y = T1y
        self.__T2x = T2x
        self.__T2y = T2y
        self.__T3x = T3x
        self.__T3y = T3y
        self.__ULx = ULx
        self.__ULy = ULy
        self.__LLx = LLx
        self.__LLy = LLy
        self.__Jx = Jx
        self.__Jy = Jy

    def get_t1x(self):
        return self.__T1x

    def get_t1y(self):
        return self.__T1y

    def get_t2x(self):
        return self.__T2x

    def get_t2y(self):
        return self.__T2y

    def get_t3x(self):
        return self.__T3x

    def get_t3y(self):
        return self.__T3y

    def get_ulx(self):
        return self.__ULx

    def get_uly(self):
        return self.__ULy

    def get_llx(self):
        return self.__LLx

    def get_lly(self):
        return self.__LLy

    def get_jx(self):
        return self.__Jx

    def get_jy(self):
        return self.__Jy
