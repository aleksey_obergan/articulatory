from domain.coordinates.coordinates_snapshot import CoordinatesSnapshot


class CoordinatesSnapshotCollection:

    def __init__(self):
        self.coordinates_snapshots = []
        self.t1x = []
        self.t1y = []
        self.t2x = []
        self.t2y = []
        self.t3x = []
        self.t3y = []
        self.ulx = []
        self.uly = []
        self.llx = []
        self.lly = []
        self.jx = []
        self.jy = []

    def add(self, coordinate):
        if not isinstance(coordinate, CoordinatesSnapshot):
            raise Exception('Wrong class instance came to CoordinatesSnapshotCollection')

        self.coordinates_snapshots.append(coordinate)
        self.t1x.append(coordinate.get_t1x())
        self.t1y.append(coordinate.get_t1y())
        self.t2x.append(coordinate.get_t2x())
        self.t2y.append(coordinate.get_t2y())
        self.t3x.append(coordinate.get_t3x())
        self.t3y.append(coordinate.get_t3y())
        self.ulx.append(coordinate.get_ulx())
        self.uly.append(coordinate.get_uly())
        self.llx.append(coordinate.get_llx())
        self.lly.append(coordinate.get_lly())
        self.jx.append(coordinate.get_jx())
        self.jy.append(coordinate.get_jy())
