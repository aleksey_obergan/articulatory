class Measurement:

    def __init__(
            self,
            measurement_name,
            phoneme_collection
    ):
        self.__measurement_name = measurement_name
        self.__phoneme_collection = phoneme_collection

    def get_phonemes_collection(self):
        return self.__phoneme_collection
