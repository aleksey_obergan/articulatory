from domain.measurement.measurement import Measurement


class MeasurementCollection:

    def __init__(self):
        self.__measurements = []

    def add(self, measurement):
        if not isinstance(measurement, Measurement):
            raise Exception('Wrong class instance came to MeasurementCollection')

        self.__measurements.append(measurement)

    def get_all(self):
        return self.__measurements
