class MeasurementsRepositoryInterface:

    def __init__(self):
        pass

    def get_measurements_collection(self):
        raise Exception('Method get_measurements_collection must be implemented')
