from domain.coordinates.coordinates_snapshot_collection import CoordinatesSnapshotCollection


class Phoneme:

    PHONEME_PAUSE_DESCRIPTION = '#'

    def __init__(
            self,
            phoneme_name,
            coordinates_collection
    ):
        self.__phoneme_name = phoneme_name

        if not isinstance(coordinates_collection, CoordinatesSnapshotCollection):
            raise Exception('Wrong class instance came to Phoneme expected CoordinatesSnapshotCollection')

        self.coordinates_collection = coordinates_collection

    def get_middle_coordinate(self):
        number_of_coordinates = len(self.coordinates_collection.coordinates_snapshots)
        middle_index = int((number_of_coordinates - 1) / 2)
        return self.coordinates_collection.coordinates_snapshots[middle_index]

    def get_phoneme_name(self):
        return self.__phoneme_name

    def get_coordinates_collection(self):
        return self.coordinates_collection

    def update(self, new_phoneme_data):
        for coordinate in new_phoneme_data.get_coordinates_collection().coordinates_snapshots:
            self.coordinates_collection.add(coordinate)
