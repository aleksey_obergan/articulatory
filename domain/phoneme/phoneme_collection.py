from domain.phoneme.phoneme import Phoneme


class PhonemeCollection:

    def __init__(self):
        self.__phonemes = []

    def add(self, phoneme):
        if not isinstance(phoneme, Phoneme):
            raise Exception('Wrong class instance came to PhonemeCollection')

        self.__phonemes.append(phoneme)

    def get_all(self):
        return self.__phonemes
