class PhonemeRepositoryInterface:

    def __init__(self):
        pass

    def get_phonemes_collection(self, raw_data):
        raise Exception('Method get_phonemes_collection must be implemented')
