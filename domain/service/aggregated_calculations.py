import numpy as np


class AggregatedCalculations:

    def calculate(results_list):
        return {
            "min": min(results_list),
            "max": max(results_list),
            "mean": np.mean(results_list),
            "std": np.std(results_list)
        }

    def get_min(self, results_list):
        return min(results_list)

    def get_max(self, results_list):
        return max(results_list)

    def get_mean(self, results_list):
        return np.mean(results_list)

    def get_standard_deviation(self, results_list):
        return np.std(results_list)
