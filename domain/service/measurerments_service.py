from domain.measurement.measurement import Measurement
from domain.phoneme.phoneme import Phoneme


class MeasurementService:

    @staticmethod
    # First pause coordinate in measurement is supposed to be neutral point
    def get_measurement_neutral_point(measurement):
        if not isinstance(measurement, Measurement):
            raise Exception('Wrong class instance came to get_measurement_neutral_point')

        for phoneme in measurement.get_phonemes_collection().get_all():
            if phoneme.get_phoneme_name() == Phoneme.PHONEME_PAUSE_DESCRIPTION:
                return phoneme.get_coordinates_collection().coordinates_snapshots[0]

        raise Exception('Measurement does not have pause phoneme')
