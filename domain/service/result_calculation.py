import numpy as np
from domain.measurement.measurement_collection import MeasurementCollection
from domain.service.aggregated_calculations import AggregatedCalculations
from domain.service.measurerments_service import MeasurementService


class ResultCalculation:

    def __init__(self):
        self.__aggregated_calculations = AggregatedCalculations()

    @staticmethod
    def calculate_lips_euclidean_distances(measurements_collection):
        if not isinstance(measurements_collection, MeasurementCollection):
            raise Exception('Wrong class instance came to EuclideanDistanceOutputBuilder')

        results = {}
        for measurement in measurements_collection.get_all():
            for phoneme in measurement.get_phonemes_collection().get_all():
                if not phoneme.get_phoneme_name() in results:
                    results[phoneme.get_phoneme_name()] = []
                coordinate = phoneme.get_middle_coordinate()
                upper_lip_point = np.array([coordinate.get_ulx(), coordinate.get_uly()])
                lower_lip_point = np.array([coordinate.get_llx(), coordinate.get_lly()])
                distance = np.linalg.norm(upper_lip_point - lower_lip_point)
                results[phoneme.get_phoneme_name()].append(distance)

        return results

    # Get min, max, mean, std from calculate_lips_euclidean_distances
    @staticmethod
    def get_euclidean_distances_aggregation(euclidean_distances):
        results = []
        for phoneme_name, results_list in euclidean_distances.items():
            aggregated_data = AggregatedCalculations.calculate(results_list)
            aggregated_data["phoneme_name"] = phoneme_name
            results.append(aggregated_data)
        return results

    @staticmethod
    def tongue_tip_displacements(measurements_collection):
        if not isinstance(measurements_collection, MeasurementCollection):
            raise Exception('Wrong class instance came to EuclideanDistanceOutputBuilder')

        results = {}
        for measurement in measurements_collection.get_all():
            neutral_coordinate_snapshot = MeasurementService.get_measurement_neutral_point(measurement)
            for phoneme in measurement.get_phonemes_collection().get_all():
                if not phoneme.get_phoneme_name() in results:
                    results[phoneme.get_phoneme_name()] = {"displacements_x": [], "displacements_y": []}
                coordinate = phoneme.get_middle_coordinate()
                displacement_x = coordinate.get_t1x() - neutral_coordinate_snapshot.get_t1x()
                displacement_y = coordinate.get_t1y() - neutral_coordinate_snapshot.get_t1y()
                results[phoneme.get_phoneme_name()]["displacements_x"].append(displacement_x)
                results[phoneme.get_phoneme_name()]["displacements_y"].append(displacement_y)
        return results

    @staticmethod
    def tongue_back_displacements(measurements_collection):
        if not isinstance(measurements_collection, MeasurementCollection):
            raise Exception('Wrong class instance came to EuclideanDistanceOutputBuilder')

        results = {}
        for measurement in measurements_collection.get_all():
            neutral_coordinate_snapshot = MeasurementService.get_measurement_neutral_point(measurement)
            for phoneme in measurement.get_phonemes_collection().get_all():
                if not phoneme.get_phoneme_name() in results:
                    results[phoneme.get_phoneme_name()] = {"displacements_x": [], "displacements_y": []}
                coordinate = phoneme.get_middle_coordinate()
                displacement_x = coordinate.get_t2x() - neutral_coordinate_snapshot.get_t2x()
                displacement_y = coordinate.get_t2y() - neutral_coordinate_snapshot.get_t2y()
                results[phoneme.get_phoneme_name()]["displacements_x"].append(displacement_x)
                results[phoneme.get_phoneme_name()]["displacements_y"].append(displacement_y)
        return results

    @staticmethod
    def jaw_displacements(measurements_collection):
        if not isinstance(measurements_collection, MeasurementCollection):
            raise Exception('Wrong class instance came to EuclideanDistanceOutputBuilder')

        results = {}
        for measurement in measurements_collection.get_all():
            neutral_coordinate_snapshot = MeasurementService.get_measurement_neutral_point(measurement)
            for phoneme in measurement.get_phonemes_collection().get_all():
                if not phoneme.get_phoneme_name() in results:
                    results[phoneme.get_phoneme_name()] = {"displacements_x": [], "displacements_y": []}
                coordinate = phoneme.get_middle_coordinate()
                displacement_x = coordinate.get_jx() - neutral_coordinate_snapshot.get_jx()
                displacement_y = coordinate.get_jy() - neutral_coordinate_snapshot.get_jy()
                results[phoneme.get_phoneme_name()]["displacements_x"].append(displacement_x)
                results[phoneme.get_phoneme_name()]["displacements_y"].append(displacement_y)
        return results

    # Get min, max, mean, std from calculate_lips_euclidean_distances
    @staticmethod
    def get_displacements_aggregation(tongue_tip_displacements):
        results = []
        for phoneme_name, displacements in tongue_tip_displacements.items():
            displacements_x_aggregated = AggregatedCalculations.calculate(displacements["displacements_x"])
            displacements_y_aggregated = AggregatedCalculations.calculate(displacements["displacements_y"])
            results.append({
                "phoneme_name": phoneme_name,
                "displacements_x_aggregated": displacements_x_aggregated,
                "displacements_y_aggregated": displacements_y_aggregated,
            })
        return results
