import os


class FileHelper:

    def __init__(self):
        pass

    @staticmethod
    def find_unique_file_names(path, extension):

        result = []
        for needle_file in os.listdir(path):
            if needle_file.endswith(extension):
                result.append(os.path.splitext(needle_file)[0])

        return result
