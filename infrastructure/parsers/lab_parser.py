from config import conig


# File isn't too big - so we can parse all of it, not line by line
class LabParser:
    LIST_INDEX_DURATION = 0
    LIST_INDEX_PHONEME_NAME = 2

    PHONEME_PAUSE = '#'

    def __init__(self):
        pass

    def parse(self, file_name):
        result = []
        read_phonemes = False
        sensors_data = self.__parse_sensors_data(file_name)
        sensors_last_read_row = 0
        previous_line = None

        with open(file_name + '.lab') as lab_file:
            for line in lab_file:
                line_as_list = line.split()
                if self._is_pause(line_as_list) and read_phonemes is False:
                    read_phonemes = True
                    # Hack to init line with zero timestamp
                    previous_line = ['0', '26', '#']
                if previous_line and read_phonemes:
                    duration = float(line_as_list[self.LIST_INDEX_DURATION])
                    previous_duration = float(previous_line[self.LIST_INDEX_DURATION])
                    duration_diff = duration - previous_duration

                    number_of_lines_to_read = round(duration_diff / conig.SENSORS_DATA_TIME_INTERVAL)
                    sensors_rows = sensors_data[sensors_last_read_row: sensors_last_read_row + number_of_lines_to_read]
                    sensors_last_read_row += number_of_lines_to_read
                    result.append({'name': self._get_phoneme_name(line_as_list), 'sensors_row': sensors_rows})

                previous_line = line_as_list

            return result

    def _is_pause(self, line):
        return self._get_phoneme_name(line) == self.PHONEME_PAUSE

    def _get_phoneme_name(self, line):
        try:
            return line[self.LIST_INDEX_PHONEME_NAME]
        except IndexError:
            return ''

    @staticmethod
    def __parse_sensors_data(file_name):
        with open(file_name + '.txt') as sensors_file:
            return sensors_file.readlines()
