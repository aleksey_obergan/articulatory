from domain.coordinates.coordinates_repository_interface import CoordinatesRepositoryInterface
from domain.coordinates.coordinates_snapshot import CoordinatesSnapshot
from domain.coordinates.coordinates_snapshot_collection import CoordinatesSnapshotCollection


class FileCoordinatesRepository(CoordinatesRepositoryInterface):
    FILE_LINE_LENGTH = 12
    T1X_INDEX = 0
    T1Y_INDEX = 1
    T2X_INDEX = 2
    T2Y_INDEX = 3
    T3X_INDEX = 4
    T3Y_INDEX = 5
    JX_INDEX = 6
    JY_INDEX = 7
    ULX_INDEX = 8
    ULY_INDEX = 9
    LLX_INDEX = 10
    LLY_INDEX = 11

    def __init__(self):
        CoordinatesRepositoryInterface.__init__(self)

    def get_coordinates_collection(self, lines):

        coordinates_snapshot_collection = CoordinatesSnapshotCollection()
        for line in lines:
            coordinates_snapshot = self.get_coordinates_snapshot(line)
            coordinates_snapshot_collection.add(coordinates_snapshot)

        return coordinates_snapshot_collection

    def get_coordinates_snapshot(self, line_data):

        data = line_data.split()
        if len(data) != self.FILE_LINE_LENGTH:
            raise Exception('File line length do not match to expected %(line)s' % {'line': line_data})

        return CoordinatesSnapshot(
            float(data[self.T1X_INDEX]),
            float(data[self.T1Y_INDEX]),
            float(data[self.T2X_INDEX]),
            float(data[self.T2Y_INDEX]),
            float(data[self.T3X_INDEX]),
            float(data[self.T3Y_INDEX]),
            float(data[self.ULX_INDEX]),
            float(data[self.ULY_INDEX]),
            float(data[self.LLX_INDEX]),
            float(data[self.LLY_INDEX]),
            float(data[self.JX_INDEX]),
            float(data[self.JY_INDEX]),
        )
