import os

from config import conig
from domain.measurement.measurement import Measurement
from domain.measurement.measurement_collection import MeasurementCollection
from domain.measurement.measurement_repository_interface import MeasurementsRepositoryInterface
from infrastructure.parsers.file_helper import FileHelper
from infrastructure.parsers.lab_parser import LabParser
from infrastructure.repository.domain.file_phoneme_repository import FilePhonemeRepository


class FileMeasurementsRepository(MeasurementsRepositoryInterface):
    NEEDED_EXTENSION = '.lab'

    def __init__(self):
        MeasurementsRepositoryInterface.__init__(self)
        self.__lab_parser = LabParser()
        self.__phoneme_repository = FilePhonemeRepository()

    def get_measurements_collection(self):
        files = FileHelper.find_unique_file_names(conig.PATH_TO_DATA, self.NEEDED_EXTENSION)
        measurements_collection = MeasurementCollection()
        for measurement_name in files:
            try:
                raw_data = self.__lab_parser.parse(os.path.join(conig.PATH_TO_DATA, measurement_name))
                phonemes_collection = self.__phoneme_repository.get_phonemes_collection(raw_data)
                measurement = self.__create_measurement(measurement_name, phonemes_collection)
                measurements_collection.add(measurement)
            except Exception as error:
                print('Error during data parsing:')
                print(error)

        return measurements_collection

    @staticmethod
    def __create_measurement(name, phonemes_collection):
        return Measurement(name, phonemes_collection)
