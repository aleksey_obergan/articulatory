from domain.phoneme.phoneme import Phoneme
from domain.phoneme.phoneme_collection import PhonemeCollection
from domain.phoneme.phoneme_repository_interface import PhonemeRepositoryInterface
from infrastructure.parsers.lab_parser import LabParser
from infrastructure.repository.domain.file_coordinates_repository import FileCoordinatesRepository


class FilePhonemeRepository(PhonemeRepositoryInterface):
    NEEDED_EXTENSION = '.lab'

    def __init__(self):
        PhonemeRepositoryInterface.__init__(self)
        self.__lab_parser = LabParser()
        self.__coordinates_repository = FileCoordinatesRepository()

    def get_phonemes_collection(self, raw_data):
        phonemes_collection = PhonemeCollection()
        for raw_data_dict in raw_data:
            phoneme_name = raw_data_dict['name']
            coordinates_data = raw_data_dict['sensors_row']
            phoneme = self.__get_phoneme(phoneme_name, coordinates_data)
            phonemes_collection.add(phoneme)

        return phonemes_collection

    def __get_phoneme(self, name, coordinates_data):
        coordinates_collection = self.__coordinates_repository.get_coordinates_collection(coordinates_data)
        return Phoneme(name, coordinates_collection)
