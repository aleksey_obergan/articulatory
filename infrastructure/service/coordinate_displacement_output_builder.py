import csv
import os

from config import conig


class CoordinateDisplacementOutputBuilder:
    def __init__(self):
        pass

    @staticmethod
    def detailed_report(filename, detailed_displacements):
        distances_file = os.path.join(conig.OUTPUT_DIRECTORY, filename + '.csv')
        with open(distances_file, 'w') as file:
            writer = csv.writer(file)

            writer.writerow(["Phoneme", "Displacement X", "Displacement Y"])
            for phoneme_name, displacements_dict in detailed_displacements.items():
                for index, displacement_x in enumerate(displacements_dict["displacements_x"]):
                    displacement_y = displacements_dict["displacements_y"][index]
                    writer.writerow([phoneme_name, displacement_x, displacement_y])

    @staticmethod
    def aggregated_report(filename, aggregated_displacements):
        distances_file = os.path.join(conig.OUTPUT_DIRECTORY, filename + '.csv')
        with open(distances_file, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(["Phoneme", "X min", "X max", "X mean", "X std", "Y min", "Y max", "Y mean", "Y std", ])
            for phoneme_aggr_data in aggregated_displacements:
                writer.writerow([
                    phoneme_aggr_data["phoneme_name"],
                    phoneme_aggr_data["displacements_x_aggregated"]["min"],
                    phoneme_aggr_data["displacements_x_aggregated"]["max"],
                    phoneme_aggr_data["displacements_x_aggregated"]["mean"],
                    phoneme_aggr_data["displacements_x_aggregated"]["std"],
                    phoneme_aggr_data["displacements_y_aggregated"]["min"],
                    phoneme_aggr_data["displacements_y_aggregated"]["max"],
                    phoneme_aggr_data["displacements_y_aggregated"]["mean"],
                    phoneme_aggr_data["displacements_y_aggregated"]["std"]
                ])
