import csv
import os

from config import conig


class EuclideanDistanceOutputBuilder:
    def __init__(self):
        pass

    @staticmethod
    def build_detailed_report(results):
        distances_file = os.path.join(conig.OUTPUT_DIRECTORY, 'euclidean_distances.csv')
        with open(distances_file, 'w') as file:
            writer = csv.writer(file)

            writer.writerow(["Phoneme", "Euclidean Distance"])
            for phoneme_name, distances in results.items():
                if phoneme_name == '#':
                    pass
                for distance in distances:
                    writer.writerow([phoneme_name, distance])

    @staticmethod
    def build_aggregated_data(results):
        distances_file = os.path.join(conig.OUTPUT_DIRECTORY, 'euclidean_distances_aggregation.csv')
        with open(distances_file, 'w') as file:
            writer = csv.writer(file)

            writer.writerow(["Phoneme", "min", "max", "mean", "std"])
            for phoneme in results:
                if phoneme["phoneme_name"] == '#':
                    pass
                writer.writerow([
                    phoneme["phoneme_name"],
                    phoneme["min"],
                    phoneme["max"],
                    phoneme["mean"],
                    phoneme["std"],
                ])
