from domain.phoneme.phoneme import Phoneme


class ResultOutput:
    def __init__(self):
        pass

    @staticmethod
    def compose_result(phoneme):
        if not isinstance(phoneme, Phoneme):
            raise Exception('Wrong class instance came to ResultOutput expected Phoneme')

        print('Phoneme %(phoneme_name)s' % {'phoneme_name': phoneme.get_phoneme_name()})
        print('        min t1x: %f' % (min(phoneme.get_coordinates_collection().t1x)))
        print('        max t1x: %f' % (max(phoneme.get_coordinates_collection().t1x)))
        print('        min t1y: %f' % (min(phoneme.get_coordinates_collection().t1y)))
        print('        max t1y: %f' % (max(phoneme.get_coordinates_collection().t1y)))

        print('-- ')
        print('        min t2x: %f' % (min(phoneme.get_coordinates_collection().t2x)))
        print('        max t2x: %f' % (max(phoneme.get_coordinates_collection().t2x)))
        print('        min t2y: %f' % (min(phoneme.get_coordinates_collection().t2y)))
        print('        max t2y: %f' % (max(phoneme.get_coordinates_collection().t2y)))

        print('-- ')
        print('        min t3x: %f' % (min(phoneme.get_coordinates_collection().t3x)))
        print('        max t3x: %f' % (max(phoneme.get_coordinates_collection().t3x)))
        print('        min t3y: %f' % (min(phoneme.get_coordinates_collection().t3y)))
        print('        max t3y: %f' % (max(phoneme.get_coordinates_collection().t3y)))

        print('-- ')
        print('        min ulx: %f' % (min(phoneme.get_coordinates_collection().ulx)))
        print('        max ulx: %f' % (max(phoneme.get_coordinates_collection().ulx)))
        print('        min uly: %f' % (min(phoneme.get_coordinates_collection().uly)))
        print('        max uly: %f' % (max(phoneme.get_coordinates_collection().ulx)))

        print('-- ')
        print('        min llx: %f' % (min(phoneme.get_coordinates_collection().llx)))
        print('        max llx: %f' % (max(phoneme.get_coordinates_collection().llx)))
        print('        min lly: %f' % (min(phoneme.get_coordinates_collection().lly)))
        print('        max lly: %f' % (max(phoneme.get_coordinates_collection().llx)))

        print('-- ')
        print('        min jx: %f' % (min(phoneme.get_coordinates_collection().jx)))
        print('        max jx: %f' % (max(phoneme.get_coordinates_collection().jx)))
        print('        min jy: %f' % (min(phoneme.get_coordinates_collection().jx)))
        print('        max jy: %f' % (max(phoneme.get_coordinates_collection().jx)))
